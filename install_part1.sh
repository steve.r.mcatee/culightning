#!/bin/bash
GREEN='\033[0;32m'
NC='\033[0m'
echo -e "${GREEN}Running install script for debian based systems (Optimized for MEAN stack server by Bitnami on Google Cloud services)${NC}"
#ufw installation
echo -e "${GREEN}Installing UFW...${NC}"
cd ~
sudo apt-get update
sudo apt-get install ufw
sudo ufw disable
sudo ufw default deny incoming 
sudo ufw default allow outgoing 
sudo ufw allow ssh  
sudo ufw allow http 
sudo ufw allow https 
sudo ufw allow 9735/tcp 
sudo ufw allow 9735/udp 
sudo ufw allow 9735 
sudo ufw enable
#bitcoin core installation
echo -e "${GREEN}Installing Bitcoin Core...${NC}"
mkdir bitcoinbin
cd bitcoinbin 
wget https://bitcoin.org/bin/bitcoin-core-0.21.1/bitcoin-0.21.1-x86_64-linux-gnu.tar.gz
tar -xzf bitcoin-0.21.1-x86_64-linux-gnu.tar.gz
cd ~
#immortal installation
echo -e "${GREEN}Installing Immortal...${NC}"
curl -s https://packagecloud.io/install/repositories/immortal/immortal/script.deb.sh | sudo bash 
sudo apt-get install immortal 
#lightning installation
echo -e "${GREEN}Installing Lightning...${NC}"
sudo apt-get update 
sudo apt-get install -y autoconf automake build-essential git libtool libgmp-dev libsqlite3-dev python3 python3-mako net-tools zlib1g-dev libsodium-dev gettext
pip3 install mako
pip3 install mrkd 
pip3 install Make
pip3 install flake8
sudo apt-get upgrade
sudo shutdown -r now


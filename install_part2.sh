#!/bin/bash
cd ~
mkdir builds
cd builds
git clone https://github.com/ElementsProject/lightning.git
cd lightning
./configure
make
cd ~
# symlink config
echo -e "${GREEN}Creating symlinks...${NC}"
export PATH=$PATH:~/bitcoinbin/bitcoin-0.21.1/bin/bitcoind
export PATH=$PATH:~/bitcoinbin/bitcoin-0.21.1/bin/bitcoin-cli
export PATH=$PATH:~/builds/lightning/lightningd
export PATH=$PATH:~/builds/lightning/cli
cd /usr/bin/
sudo ln -s ~/bitcoinbin/bitcoin-0.21.1/bin/bitcoind bitcoind
sudo ln -s ~/bitcoinbin/bitcoin-0.21.1/bin/bitcoin-cli bitcoin-cli
sudo ln -s ~/builds/lightning/lightningd/lightningd lightningd
sudo ln -s ~/builds/lightning/cli/lightning-cli lightning-cli
source ~/.bashrc
source ~/.profile
echo -e "${GREEN}Finishing up...${NC}"
cd ~
sudo npm i -g npm
sudo npm install -g forever
sudo npm install -g yarn 
#lighting charge has errors when installed with NPM, we use yarn instead
sudo yarn global add lightning-charge
cd ~
mkdir lightning-logs
mkdir chargedb
mkdir .lightning
cp ~/lightning-installer/scripts/gcs_mean_bitnami/config ~/.lightning/
#immortal bitcoind --daemon
#echo -e "${GREEN}Installation completed! Now wait 12+ hours for bitcoin node to fully sync!${NC}"
